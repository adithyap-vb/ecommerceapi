const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  productName: { type: String, required: true },
  productDescription: String,
  productThumbnail: String,
  productGallery: Array,
  productBasePrice: Number,
  productSellPrice: Number,
  productCategoryName: String,
  productTags: Array,
  productAdditionalInfo: String,
});

module.exports = mongoose.model("product", productSchema, "Products");
