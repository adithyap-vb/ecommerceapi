const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roleSchema = new Schema({
  roleName: { type: String, required: true },
  roleSlug: String,
});

module.exports = mongoose.model("role", roleSchema, "Roles");
