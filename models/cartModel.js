const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cartSchema = new Schema({
  products: Array,
  user: String,
  productQty: Number,
  basePrice: Number,
  sellPrice: Number,
  total: Number,
});

module.exports = mongoose.model("cart", cartSchema, "Carts");
