const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({
  userIds: String,
  products: Array,
  totalItems: Number,
  billingAddress: String,
  shippingAddress: String,
  transactionStatus: String,
  paymentMode: String,
  paymentStatus: String,
  orderStatus: String,
});

module.exports = mongoose.model("order", orderSchema, "Orders");
