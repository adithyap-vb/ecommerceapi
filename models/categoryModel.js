const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const categorySchema = new Schema({
  categoryName: { type: String, required: true },
  categorySlug: String,
  categoryImage: String,
  categoryDescription: String,
});

module.exports = mongoose.model("category", categorySchema, "Categories");
