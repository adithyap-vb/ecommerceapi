const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tagSchema = new Schema({
  tagName: String,
  tagSlug: String,
});
module.exports = mongoose.model("tag", tagSchema, "Tags");
