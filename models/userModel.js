const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  userFirstName: String,
  userLastName: String,
  userEmail: String,
  userProfileImage: String,
  userRole: String,
});

module.exports = mongoose.model("user", userSchema, "Users");
