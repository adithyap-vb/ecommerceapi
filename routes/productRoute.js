const express = require("express");
const router = express.Router();
const mongodb = require("mongodb");
const productModel = require("../models/productModel");

//This route is used to get all the product documents from the database
router.get("/", (req, res) => {
  productModel.find({}, (err, products) => {
    if (err) {
      console.log(err);
    } else {
      res.send(products);
    }
  });
});

//This route is used to search products by category
router.get("/searchByCategory/:category", (req, res) => {
  productModel.find(
    { productCategoryName: req.params.category },
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route is used to get product by name
router.get("/searchByName/:productName", (req, res) => {
  console.log(req.params.productName);
  productModel.find({ productName: req.params.productName }, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to get products by tags
// router.get("/:tag",(req,res)=>{
//   productModel.find({

//   })
// })

//This route is used to create a product
router.post("/createProduct", (req, res) => {
  const product = new productModel();
  product.productName = req.body.productName;
  product.productDescription = req.body.productDescription;
  product.productThumbnail = req.body.productThumbnail;
  product.productGallery = req.body.productGallery;
  product.productBasePrice = req.body.productBasePrice;
  product.productSellPrice = req.body.productSellPrice;
  product.productCategoryName = req.body.productCategoryName;
  product.productTags = req.body.productTags;
  product.productAdditionalInfo = req.body.productAdditionalInfo;

  product.save((err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update product by _id
router.patch("/updateProduct/:id", (req, res) => {
  productModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route is used to delete product by _id
router.delete("/deleteProduct/:id", (req, res) => {
  productModel.deleteOne(
    { _id: mongodb.ObjectId(req.params.id) },
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});
module.exports = router;
