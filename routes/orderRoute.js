const express = require("express");
const router = express.Router();
const mongodb = require("mongodb");
const orderModel = require("../models/orderModel");

//This route is used to get all the order documents from the database
router.get("/", (req, res) => {
  orderModel.find({}, (err, orders) => {
    if (err) {
      console.log(err);
    } else {
      res.send(orders);
    }
  });
});

//This route is used to create order using data from req.body
router.post("/createOrder", (req, res) => {
  const order = new orderModel();
  order.userIds = req.body.userIds;
  order.products = req.body.products;
  order.totalItems = req.body.totalItems;
  order.billingAddress = req.body.billingAddress;
  order.shippingAddress = req.body.shippingAddress;
  order.transactionStatus = req.body.transactionStatus;
  order.paymentMode = req.body.paymentMode;
  order.paymentStatus = req.body.paymentStatus;
  order.orderStatus = req.body.orderStatus;

  order.save((err, doc) => {
    if (err) {
      console.log(`err is ${err}`);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update order using _id
router.patch("/updateOrder/:id", (req, res) => {
  orderModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route deletes order by _id
router.delete("/deleteOrder/:id", (req, res) => {
  orderModel.deleteOne({ _id: mongodb.ObjectId(req.params.id) }, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

module.exports = router;
