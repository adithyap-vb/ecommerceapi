const express = require("express");
const mongodb = require("mongodb");
const router = express.Router();
const categoryModel = require("../models/categoryModel");

//This route is used to get all category documents from the database
router.get("/", (req, res) => {
  categoryModel.find({}, (err, categories) => {
    if (err) {
      console.log(err);
    } else {
      res.send(categories);
    }
  });
});

//This route is used to create category using data from req.body
router.post("/createCategory", (req, res) => {
  const category = new categoryModel();
  category.categoryName = req.body.categoryName;
  category.categorySlug = req.body.categorySlug;
  category.categoryImage = req.body.categoryImage;
  category.categoryDescription = req.body.categoryDescription;

  category.save((err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update category using _id
router.patch("/updateCategory/:id", (req, res) => {
  categoryModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route is used to delete category by _id
router.delete("/deleteCategory/:id", (req, res) => {
  categoryModel.deleteOne(
    { _id: mongodb.ObjectId(req.params.id) },
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});
module.exports = router;
