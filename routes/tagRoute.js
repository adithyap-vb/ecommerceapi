const express = require("express");
const router = express.Router();
const mongodb = require("mongodb");
const tagModel = require("../models/tagModel");

//This route is used to get all the tags documents from the database
router.get("/", (req, res) => {
  tagModel.find({}, (err, tags) => {
    if (err) {
      console.log(err);
    } else {
      res.send(tags);
    }
  });
});

//This route is used to create tag using data from req.body
router.post("/createTag", (req, res) => {
  const tag = new tagModel();
  tag.tagName = req.body.tagName;
  tag.tagSlug = req.body.tagSlug;
  tag.save((err, doc) => {
    if (err) {
      console.log(`err is ${err}`);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update tag using _id
router.patch("/updateTag/:id", (req, res) => {
  tagModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route deletes tag by _id
router.delete("/deleteTag/:id", (req, res) => {
  tagModel.deleteOne({ _id: mongodb.ObjectId(req.params.id) }, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route deleted all tags
router.delete("/deleteAllTags", (req, res) => {
  tagModel.deleteMany({}, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

module.exports = router;
