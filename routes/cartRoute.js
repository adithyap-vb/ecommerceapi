const express = require("express");
const router = express.Router();
const mongodb = require("mongodb");
const cartModelNew = require("../models/cartModelNew");

//This route is used to get all the cart documents from the database
router.get("/", (req, res) => {
  cartModelNew.find({}, (err, carts) => {
    if (err) {
      console.log(err);
    } else {
      res.send(carts);
    }
  });
});

//This route is used to create cart using data from req.body
router.post("/createCart", (req, res) => {
  const cart = new cartModelNew();
  cart.products = req.body.products;
  cart.user = req.body.user;
  cart.productQty = req.body.productQty;
  cart.basePrice = req.body.basePrice;
  cart.sellPrice = req.body.sellPrice;
  cart.total = req.body.total;

  cart.save((err, doc) => {
    if (err) {
      console.log(`err is ${err}`);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update cart using _id
router.patch("/updateCart/:id", (req, res) => {
  cartModelNew.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route deletes cart by _id
router.delete("/deleteCart/:id", (req, res) => {
  cartModelNew.deleteOne(
    { _id: mongodb.ObjectId(req.params.id) },
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

module.exports = router;
