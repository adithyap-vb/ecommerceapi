const express = require("express");
const mongodb = require("mongodb");
const router = express.Router();
const roleModel = require("../models/roleModel");

//This route is used to get all the role documents
router.get("/", (req, res) => {
  roleModel.find({}, (err, roles) => {
    if (err) {
      console.log(err);
    } else {
      res.send(roles);
    }
  });
});

//This route is used to create role using data from req.body
router.post("/createRole", (req, res) => {
  const role = new roleModel();
  role.roleName = req.body.roleName;
  role.roleSlug = req.body.roleSlug;
  role.save((err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update role using _id
router.patch("/updateRole/:id", (req, res) => {
  roleModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This route deletes role by _id
router.delete("/deleteRole/:id", (req, res) => {
  roleModel.deleteOne({ _id: mongodb.ObjectId(req.params.id) }, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

//This route deletes all roles
router.delete("/deleteAllRoles", (req, res) => {
  roleModel.deleteMany({}, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

module.exports = router;
