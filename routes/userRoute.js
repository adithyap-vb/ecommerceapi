const express = require("express");
const router = express.Router();
const mongodb = require("mongodb");
const userModel = require("../models/userModel");

//This route gets all users documents from the database
router.get("/", (req, res) => {
  userModel.find({}, (err, users) => {
    if (err) console.log(err);
    res.send(users);
  });
});

//This route is used to create user using data from req.body
router.post("/createUser", (req, res) => {
  const user = new userModel();
  user.userFirstName = req.body.userFirstName;
  user.userLastName = req.body.userLastName;
  user.userEmail = req.body.userEmail;
  user.userProfileImage = req.body.userProfileImage;
  user.userRole = req.body.userRole;
  user.save((err, doc) => {
    if (err) {
      console.log(`err is ${err}`);
    } else {
      res.send(doc);
    }
  });
});

//This route is used to update user by _id
router.patch("/updateUser/:id", (req, res) => {
  userModel.findOneAndUpdate(
    { _id: mongodb.ObjectId(req.params.id) },
    JSON.parse(JSON.stringify(req.body)),
    (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        res.send(doc);
      }
    }
  );
});

//This Route deletes user by _id
router.delete("/deleteUser/:id", (req, res) => {
  userModel.deleteOne({ _id: mongodb.ObjectId(req.params.id) }, (err, doc) => {
    if (err) {
      console.log(err);
    } else {
      res.send(doc);
    }
  });
});

module.exports = router;
