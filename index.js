const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const app = express();
const secret = "SecretKey";
mongoose
  .connect("mongodb://127.0.0.1:27017/ecommerce")
  .then(() => console.log(`mongodb connected!`));

const userRoute = require("./routes/userRoute");
const tagRoute = require("./routes/tagRoute.js");
const roleRoute = require("./routes/roleRoute");
const cartRoute = require("./routes/cartRoute");
const categoryRoute = require("./routes/categoryRoute");
const orderRoute = require("./routes/orderRoute");
const productRoute = require("./routes/productRoute");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/login", (req, res, next) => {
  //console.log(`req.headers.authorization is ${req.headers.authorization}`);
  let isAuthorized = false;
  if (req.headers.authorization === undefined) {
    let user = {
      email: "adithya@email.com",
      password: "password",
    };

    let token = jwt.sign(user, secret);
    res.sendStatus(200).send(token);
  } else {
    let token = req.headers.authorization.split(" ")[1];

    jwt.verify(token, secret, (err, res) => {
      if (err) {
        console.log(`err is ${err}`);
      } else {
        console.log(`res is ${JSON.stringify(res)}`);
        isAuthorized = true;

        app.use("/users", userRoute);
        app.use("/tags", tagRoute);
        app.use("/roles", roleRoute);
        app.use("/carts", cartRoute);
        app.use("/categories", categoryRoute);
        app.use("/orders", orderRoute);
        app.use("/products", productRoute);
      }
    });
  }
  return isAuthorized ? res.send("Log In Complete") : res.send("Please Log In");
  next();
});

app.listen(8080);
